package main.java;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Andrew Pleas on 8/20/2016.
 */
public class DeckImpl implements Deck {
    private ArrayList<Card> deck;
    public DeckImpl(){
        deck = new ArrayList<>();
    }

    public ArrayList<Card> getDeck() {
        return deck;
    }

    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }
    @Override
    public void create(int numberOfSuits, int numberOfRanks) throws IllegalArgumentException {
        if(numberOfSuits <= 0 || numberOfRanks <=0){
            throw new IllegalArgumentException("Number of Ranks : "+ numberOfRanks + " and Number of Suits : "+ numberOfSuits + " must be greater than 0");
        }
        deck = new ArrayList<Card>();
        ArrayList<Suit> suitList =new ArrayList<>();
        suitList.add(Suit.CLOVER);
        suitList.add(Suit.DIAMOND);
        suitList.add(Suit.HEART);
        suitList.add(Suit.SPADE);

        for(int i = 1; i <= numberOfRanks; i ++){

            for (int j = 0; j < numberOfSuits; j++){
                deck.add(new Card (suitList.get(j), i));
            }
        }

    }

    @Override
    public void shuffle() {
        Collections.shuffle(deck);
/*
        Random rnd = ThreadLocalRandom.current();
        for (int i = deck.size() - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            Card card = deck.get(index);
            deck.add(index, deck.get(i));
            deck.add(i, card);
        }
        */
    }

    public void addCard(Card card){
        deck.add(card);

    }

    @Override
    public Card deal() {
        if(getDeck().size() >0) {
            return deck.remove(0);
        }else return null;
    }
}
