package main.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andrew Pleas on 8/20/2016.
 */
public class War {

    private  Map<Integer, DeckImpl> gameMap;
    private List<Card> prize;



    private int highestValue;
    private DeckImpl deck;
    public War(){
        gameMap  = new HashMap<>();
        prize = new ArrayList<>();
        highestValue = 0;
        deck = new DeckImpl();
    }
    /**
     * play method starts game. Uses a while loop to play until gameMap's size is 1 and there is a winner.
     * @param numberOfSuits int - number of suits for the deck, numberOfRanks int- number of card values
     * for the deck, numberOfPlayers int - number of players for the game.
     * @return Nothing.
     */
    public void play( int numberOfSuits, int numberOfRanks, int numberOfPlayers ) {
        deck.create(numberOfSuits, numberOfRanks);
        deck.shuffle();
        boolean over = false;
        createPlayerHands(numberOfPlayers, deck);
        while (!over) {
            List<Integer> loosers = getLoosers();
            for (Integer looser : loosers) {
                gameMap.remove(looser);

            }
            if (gameMap.size() == 1) {
                for(Integer num : gameMap.keySet()){
                    System.out.println("Player " + num + "  has won the game!!!!");
                }
                over = true;
            }
            if(!over) {
                Map<Integer, Card> cardIndexMap = playRound();
                Map<Integer, Card> winners = getRoundWinners(cardIndexMap);
                highestValue = 0;
                if (winners.size() == 1) {
                    for(Integer num : winners.keySet()) {
                        gameMap.get(num).getDeck().addAll(prize);

                        System.out.println("Player  " + num + "  won the "+prize.size() +" card pot!!!  ");
                    }
                    prize = new ArrayList<>();
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {
                    System.out.println("Lets go to WAR !!!!!!!!!!!!!! ");
                    playWar(winners);
                }
            }
        }


    }

    /**
     * getLoosers method checks gameMap to see if any of the players decks are empty
     * If empty key Integer is stored in a List.
     * @return Integer List of all keys that contaion empty decks in gameMap.
     */
    public List<Integer> getLoosers() {
        List<Integer> loosers = new ArrayList<>();
        for (Integer num : gameMap.keySet()) {
            if (gameMap.get(num).getDeck().size() == 0) {
                loosers.add(num);
                System.out.println("Player " + num + "  Has lost the game :( ");

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }


        }
        return loosers;
    }

    /**
     * playWar for each player up to 3 cards are dealt into the prize pot and the last one is
     * played against the others.
     * @param cardsMap, Map containing all of the players who tied in playRound() method.
     * @return Nothing.
     */
    private void playWar(Map<Integer, Card> cardsMap) {
       // Map<Integer, Card> cardsMap = new HashMap<>();
        int max = 0;
        for(Integer win: cardsMap.keySet()){
            int size = gameMap.get(win).getDeck().size();
            if(size > 4) {
                for (int i = 0; i < 3; i++) {
                    prize.add(gameMap.get(win).deal());
                }
                Card card = (gameMap.get(win).deal());
                prize.add(card);
                cardsMap.put(win, card);
                if (card.getNumber() > max) {
                    max = card.getNumber();
                }
            }else if (size > 0){
                for (int i = 0; i < size -1; i++) {
                    prize.add(gameMap.get(win).deal());
                }
                Card card = (gameMap.get(win).deal());
                cardsMap.put(win, card);
                prize.add(card);
                if (card.getNumber() > max) {
                    max = card.getNumber();
                }

            }else{
                Card card = cardsMap.get(win);
                if(card.getNumber()> highestValue){
                max = card.getNumber();
                }
            }

        }
        highestValue = max;
        for(Integer num: cardsMap.keySet()){
            System.out.println("player  " + num + "  is playing a " + cardsMap.get(num).getNumber() + "  of " + cardsMap.get(num).getSuit()  + "S in the battle");
            System.out.println(highestValue);
        }
        Map<Integer, Card> warWinner = getRoundWinners(cardsMap);
        if(warWinner.size() == 1){
            for(Integer num : warWinner.keySet()) {
                gameMap.get(num).getDeck().addAll(prize);

               System.out.println("Player  " + num + "  Won the battle and gets the "+ prize.size() +" card pot, and now has a deck size of " + gameMap.get(num).getDeck().size());
            }
            prize = new ArrayList<>();
            highestValue = 0;
            try {
                Thread.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }else{
            playWar(warWinner);
        }
    }


    /**
     * getRoundWinners makes a list of cards that are greater than or equal to the highest value.
     * @param indexOfCardsMap Map containing the key Integer and Card played by each player.
     * @return Map containing the key Integer and Card played by the winners.
     */
    public  Map<Integer, Card> getRoundWinners(Map<Integer, Card> indexOfCardsMap) {
        Map<Integer, Card> roundWinners = new HashMap<>();
        List<Integer> winners = new ArrayList<>();
        for(Integer num : indexOfCardsMap.keySet())
            if (indexOfCardsMap.get(num).getNumber() >= highestValue) {
                roundWinners.put(num, indexOfCardsMap.get(num));
                winners.add(num);
            }
        return roundWinners;
    }

    /**
     * playRound stores all cards played by each player in a Map with each players Integer key
     * and card.
     * @return Map containing the key Integer and Card played by the winners.
     */
    public  Map<Integer, Card> playRound() {
        Map<Integer, Card> cardIndexMap = new HashMap<>();
        for (Integer player : gameMap.keySet()) {
            if (gameMap.get(player).getDeck().size() > 0) {
                Card card = gameMap.get(player).deal();
                System.out.println("Player  " + player + "  is playing a " + card.getNumber() + "  : " + card.getSuit()  + " and has a deck size of: "  + gameMap.get(player).getDeck().size());
                prize.add(card);
                cardIndexMap.put(player, card);
                if (card.getNumber() > highestValue) {
                    highestValue = card.getNumber();
                }
            }
        }

        return cardIndexMap;

    }
    /**
     * createPlayerHands deals a hand to each player in the game from the deck.
     * @param players - the number of players, deck - cards to be dealt
     * @return Nothing.
     */
    public void createPlayerHands(int players, DeckImpl deck) {
        for (int i = 1; i <= players; i++){
            gameMap.put(i, new DeckImpl());
        }
        int count = 1;
        while(deck.getDeck().size()>0) {
            gameMap.get(count).addCard(deck.deal());
            if (count < players) {
                count++;
            } else {
                count = 1;
            }
        }
    }
    /**
     * getGameMap method.
     * @return gameMap a map of all of the players and their hands.
     */
    public Map<Integer, DeckImpl> getGameMap() {
        return gameMap;
    }

    /**
     * setGameMap method.
     * @param gameMap, sets gameMap to the passed in map
     * @return Nothing
     */
    public void setGameMap(Map<Integer, DeckImpl> gameMap) {
        this.gameMap = gameMap;
    }

    /**
     * setHighestValue method.
     * @param highestValue int, sets highestValue to the passed int
     * @return Nothing
     */
    public void setHighestValue(int highestValue) {
        this.highestValue = highestValue;
    }

    /**
     * getHighestValue.
     * @return highestValue
     */
    public int getHighestValue(){
        return highestValue;
    }


    public static void main(String[] args) {

    War war = new War();
    war.play(4, 13, 5);




    }
}

