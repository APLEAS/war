package main.java;


/**
 * Created by Andrew Pleas on 8/20/2016.
 */
public interface Deck {

    /* Create the deck of cards */

    public void create( int numberOfSuits, int numberOfRanks );


/* Shuffle the deck */

    public void shuffle();


/* deal a card from the deck */

    public Card deal();
}
