package main.java;

/**
 * Created by Andrew Pleas on 8/20/2016.
 */
public enum Suit {
    SPADE, HEART, DIAMOND, CLOVER
}
