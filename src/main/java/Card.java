package main.java;

/**
 * Created by Andrew Pleas on 8/20/2016.
 */
public class Card {
    private Suit suit;
    private int number;
    public Card(Suit suit, int number){
        setSuit(suit);
        setNumber(number);

    }
    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
