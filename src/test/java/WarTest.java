package test.java;

import main.java.Card;
import main.java.Suit;
import org.junit.Before;
import org.junit.Test;
import main.java.DeckImpl;
import main.java.War;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by Andrew Pleas on 8/20/2016.
 */

public class WarTest {

    private War war;
    private DeckImpl deck;
    private Map<Integer, DeckImpl> gameMap;



    @Before
    public void initialize()
    {
        deck = new DeckImpl();
        deck.create(3, 13);
        deck.shuffle();
        gameMap = createGameMap(3,deck);
        war = new War();

    }

    @Test
    public void deckSizeTest() {
        DeckImpl deck = new DeckImpl();
        deck.create(4, 13);

        assertEquals(52, deck.getDeck().size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void createDeckErrorTest() throws Exception {
        DeckImpl deck2 = new DeckImpl();
        deck2.create( -5, 10);
    }
    @Test(expected=IllegalArgumentException.class)
    public void createDeckErrorTest2() throws Exception {
        DeckImpl deck2 = new DeckImpl();
        deck2.create( 5, -10);
    }
    @Test(expected=IllegalArgumentException.class)
    public void createDeckErrorTest3() throws Exception {
        DeckImpl deck2 = new DeckImpl();
        deck2.create( -5, -10);
    }

    @Test
    public void getLooserTest() throws Exception {
        war.setGameMap(gameMap);
        List<Integer> loosers = war.getLoosers();
        assertEquals(0, loosers.size());
    }

    @Test
    public void getLooserTest2() throws Exception {
        gameMap.put(1, new DeckImpl());
        war.setGameMap(gameMap);
        List<Integer> loosers = war.getLoosers();
        assertEquals(1, loosers.size());
    }

    @Test
    public void getRoundWinnersTest() throws Exception {
        war.setHighestValue(10);
        Map<Integer, Card> hand = new HashMap<>();
        hand.put(1 , new Card(Suit.CLOVER, 10));
        hand.put(2 , new Card(Suit.DIAMOND, 10));
        hand.put(3 , new Card(Suit.HEART, 5));
        Map<Integer, Card>winners = war.getRoundWinners(hand);
        assertEquals(2, winners.size());
    }


    @Test
    public void playRoundTest() throws Exception {
        war.setGameMap(gameMap);
        Map<Integer, Card> playedCardMap = war.playRound();
        assertEquals(3, playedCardMap.size());
    }

    @Test
    public void playRoundTest2() throws Exception {
        gameMap.get(1).getDeck().add(0 ,new Card(Suit.DIAMOND, 13));
        war.setGameMap(gameMap);
        war.playRound();
        int highValue = war.getHighestValue();

        assertEquals(13, highValue);
    }

    @Test
    public void createPlayerHandTest() throws Exception {
        war.createPlayerHands(4, deck);


        assertEquals(4, war.getGameMap().size());
    }


    @Test
    public void play() throws Exception {
        DeckImpl deck = new DeckImpl();
        deck.create(1, 1);

        assertEquals(1, deck.getDeck().size());
    }

    @Test
    public void main() throws Exception {

    }

    /**
     * createGameMap creates a map that contains each player and their hand.
     * @param players int - number of players, DeckImple deck of cards
     * @return Map containing players and their hands.
     */
    private Map<Integer, DeckImpl> createGameMap(int players, DeckImpl deck) {
        Map<Integer, DeckImpl> gameMap = new HashMap<>();
        for (int i = 1; i <= players; i++){
            gameMap.put(i, new DeckImpl());

        }
        int count = 1;
        while(deck.getDeck().size()>0) {
            gameMap.get(count).addCard(deck.deal());
            if (count < players) {
                count++;
            } else {
                count = 1;
            }
        }
        return gameMap;
    }



}
